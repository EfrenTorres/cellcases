package com.example.cellcases;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ModificarProvedor extends AppCompatActivity {

    EditText edtRsocial, edtEncargado, edtTelefono, edtCorreo;
    TextView edtHorario;
    Button btnActualizarProv;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_provedor);

        //recibo nombre desde el main
        Bundle bundle = getIntent().getExtras();
        String nombre = bundle.getString("TITULO");

        //asignaciones
        edtRsocial=(EditText)findViewById(R.id.etxtRsocialProv2);
        edtEncargado=(EditText)findViewById(R.id.etxtEncargadoProv2);
        edtTelefono=(EditText)findViewById(R.id.etxtTelefonoProv2);
        edtCorreo=(EditText)findViewById(R.id.etxtCorreoProv2);
        edtHorario=(TextView)findViewById(R.id.etxtHorarioProv2);
        btnActualizarProv=(Button)findViewById(R.id.btnActProv);

        try (GestorBD bd = new GestorBD(getApplicationContext())) {
                String buscar = nombre;
                String[] datos;
                datos=bd.buscaProvedor(buscar);
                //mostrar la info almacenada en datos resultado de la consulta
                edtRsocial.setText(datos[0]);
                edtEncargado.setText(datos[1]);
                edtTelefono.setText(datos[2]);
                edtCorreo.setText(datos[3]);
                edtHorario.setText(datos[4]);

                // id para titulo del producto
                id = nombre;

            btnActualizarProv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //verificar si los campos estan vacios
                    if (edtRsocial.getText().toString().isEmpty() || edtEncargado.getText().toString().isEmpty() || edtTelefono.getText().toString().isEmpty() || edtCorreo.getText().toString().isEmpty() || edtHorario.getText().toString().isEmpty()) {
                        Toast.makeText( ModificarProvedor.this, "ASEGURATE DE LLENAR TODOS LOS CAMPOS", Toast.LENGTH_LONG).show();
                    }else{
                        // guardar datos
                        bd.actualizarProvedor(id,edtRsocial.getText().toString(), edtEncargado.getText().toString(), edtTelefono.getText().toString(), edtCorreo.getText().toString(), edtHorario.getText().toString());
                        Toast.makeText(getApplicationContext(), "SE HA AÑADIDO UN NUEVO PROVEDOR", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }

            });
        }
    }
}