package com.example.cellcases;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Compra extends AppCompatActivity {

    TextView txtTitulo, txtMarca, txtPrecio, txtDesc;
    Button btnComprar;
    String id;
    ImageView imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compra);

        //recibo nombre desde el main
        Bundle bundle = getIntent().getExtras();
        String nombre = bundle.getString("TITULO");

        //asignar los edit text
        txtTitulo=(TextView)findViewById(R.id.txtModeloCl);
        txtMarca=(TextView)findViewById(R.id.txtMarcaStore);
        txtPrecio=(TextView)findViewById(R.id.txtPrecioCl);
        txtDesc=(TextView)findViewById(R.id.txtDescStore);
        imagen=(ImageView)findViewById(R.id.ivMarcaStore);
        btnComprar=(Button)findViewById(R.id.btnLlamarCliente);

        // configuracion y conexion a bd
        try (GestorBD bd = new GestorBD(getApplicationContext())) {
            String buscar = nombre;
            String[] datos;
            datos=bd.buscaProducto(buscar);
            //mostrar la info almacenada en datos resultado de la consulta
            txtTitulo.setText(datos[0]);
            txtMarca.setText(datos[1]);
            txtPrecio.setText(datos[2]);
            txtDesc.setText(datos[3]);

            // id para titulo del producto
            id = nombre;

            //Marcas logos
            if(txtMarca.getText().equals("HUAWEI")){
                imagen.setImageResource(R.drawable.huawei);
            }else if(txtMarca.getText().equals("IPHONE")){
                imagen.setImageResource(R.drawable.iphone);
            }else if(txtMarca.getText().equals("LG")){
                imagen.setImageResource(R.drawable.lg);
            }else if(txtMarca.getText().equals("MOTOROLA")){
                imagen.setImageResource(R.drawable.moto);
            }else if(txtMarca.getText().equals("SAMSUNG")){
                imagen.setImageResource(R.drawable.samsung);
            }else if(txtMarca.getText().equals("SONY")){
                imagen.setImageResource(R.drawable.sony);
            }else if(txtMarca.getText().equals("XIAOMI")){
                imagen.setImageResource(R.drawable.xiaomi);
            }else if(txtMarca.getText().equals("ZTE")){
                imagen.setImageResource(R.drawable.zte);
            }

            // declaracion de botones
            btnComprar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Compra.this, Pagar.class);
                    intent.putExtra("TITULO", id);
                    startActivity(intent);
                    finish();
                }

            });

        }
    }
}