package com.example.cellcases;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class GestorBD extends SQLiteOpenHelper {
    private static final String NOMBRE_BD = "myclocksbd";
    private static final int VERSION_BD = 1;
    private static final String TABLA_PRODUCTO = "CREATE TABLE PRODUCTO(TITULO TEXT PRIMARY KEY, MARCA TEXT, PRECIO TEXT, DESCRIPCION TEXT)";
    private static final String TABLA_PROVEDOR = "CREATE TABLE PROVEDOR(RS TEXT PRIMARY KEY, ENCARGADO TEXT, TELEFONO TEXT, CORREO TEXT, HORARIO TEXT)";
    private static final String TABLA_COMPRAS = "CREATE TABLE COMPRAS(NOMBRE TEXT PRIMARY KEY, TELEFONO TEXT, DIRECCION TEXT, TARJETA TEXT, PRODUCTO TEXT, PRECIO TEXT)";

    public GestorBD (Context context){
        super(context, NOMBRE_BD, null, VERSION_BD);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(TABLA_PRODUCTO);
        sqLiteDatabase.execSQL(TABLA_PROVEDOR);
        sqLiteDatabase.execSQL(TABLA_COMPRAS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLA_PRODUCTO);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLA_PROVEDOR);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLA_COMPRAS);
        sqLiteDatabase.execSQL(TABLA_PRODUCTO);
        sqLiteDatabase.execSQL(TABLA_PROVEDOR);
        sqLiteDatabase.execSQL(TABLA_COMPRAS);
    }
    //datos predefinidos
    public void agregarPre(){
        SQLiteDatabase bd=getWritableDatabase();
        if (bd!=null){
            bd.execSQL("delete from PRODUCTO");
            bd.execSQL("delete from PROVEDOR");
            bd.execSQL("delete from COMPRAS");
            bd.execSQL("INSERT INTO PRODUCTO VALUES('CARCASA USO RUDO', 'MOTOROLA', '550','FUNDA DE POLIMERO RESISTENTE')");
            bd.execSQL("INSERT INTO PRODUCTO VALUES('FUNDA CASUAL', 'LG', '180','FUNDA BASICA CON DISEÑO DE MIKEY MOUSE')");
            bd.execSQL("INSERT INTO PRODUCTO VALUES('A PRUEBA DE AGUA', 'XIAOMI', '800','CARCASA PROTECTORA SUMERGIBLE HASTA 10 000 PIES')");
            bd.execSQL("INSERT INTO PRODUCTO VALUES('FUNDA COLORES', 'SONY', '270','FUNDA CASUAL CON COLORES TORNASOL')");
            bd.execSQL("INSERT INTO PRODUCTO VALUES('INDESTRUCTIBLE H', 'HUAWEI', '900','FUNDA A PRUEBA DE FUEGO, SUMERGBLE, SOPORTA HASTA 3 TONELADAS')");
            bd.execSQL("INSERT INTO PRODUCTO VALUES('FUNDA TRANSPARENTE', 'IPHONE', '100','FUNDA INVISIBLE PROTECTORA, MUY LIGERA')");
            bd.execSQL("INSERT INTO PRODUCTO VALUES('FUNDA MINIMALISTA', 'ZTE', '123','RESISTENTE A CAIDAS, POLIMERO TIPO GOMA')");
            bd.execSQL("INSERT INTO PRODUCTO VALUES('DISEÑO LIBRE', 'SAMSUNG', '1800','FUNDA CON CASES INTERCAMBIABLES, 12 COLORES DISTINTOS Y MAS DE 80 COMBINACIONES')");
            bd.execSQL("INSERT INTO PROVEDOR VALUES('MOTOROLA','DIEGO SAVIN SUAREZ','4585251545','motorola@gmail.com','24/7')");
            bd.execSQL("INSERT INTO PROVEDOR VALUES('LG','VICTOR MARTINEZ OJEDA','5212458578','lg@gmail.com','TODA LA SEMANA HORARIO AM')");
            bd.execSQL("INSERT INTO PROVEDOR VALUES('XIAOMI','SAMANTA BARRERA OCHOA','6563265158','xaomi@mail.com','LUNES A JUEVES 6 AM A 7 PM')");
            bd.execSQL("INSERT INTO COMPRAS VALUES('VANESSA PEÑAVERA ROSAS','7885963256','CHIAPAS CAPITAL EDIFICIO 3 PISO 1','67584938','FUNDA MONOCROMATICA','380')");
            bd.execSQL("INSERT INTO COMPRAS VALUES('CARINA LEON GONZALES','6857496857','LINDA VISTA CASA AZUL CALLE DELICIAS','45565476','FUNDA AZUL','200')");
            bd.close();
        }
    }

    // CRUD PARA PRODUCTOS
    public void agregarProducto(String titulo, String marca, String precio, String descripcion){
        SQLiteDatabase bd=getWritableDatabase();
        if (bd!=null){
            bd.execSQL("INSERT INTO PRODUCTO VALUES('"+titulo+"', '"+marca+"', '"+precio+"','"+descripcion+"')");
            bd.close();
        }
    }

    public void actualizarProducto(String nombre, String titulo, String marca, String precio, String descripcion){
        SQLiteDatabase bd=getWritableDatabase();
        if (bd!=null){
            bd.execSQL("UPDATE PRODUCTO SET titulo ='"+titulo+"', marca ='"+marca+"', precio ='"+precio+"', descripcion ='"+descripcion+"' WHERE titulo ='"+nombre+"'");
            bd.close();
        }
    }

    public void eliminarProducto(String id){
        SQLiteDatabase bd=getWritableDatabase();
        if (bd!=null){
            bd.execSQL("DELETE FROM PRODUCTO WHERE titulo='"+id+"'");
            bd.close();
        }
    }
    public ArrayList datos_lvProducto(){
        ArrayList<String> lista = new ArrayList<>();
        SQLiteDatabase bd = this.getReadableDatabase();
        String q = "SELECT * FROM PRODUCTO";
        Cursor registros = bd.rawQuery(q,null);
        if (registros.moveToFirst()){
            do {
                String producto = registros.getString(0);
                lista.add(producto);
            }while(registros.moveToNext());
        }
        return lista;
    }

    public  String[] buscaProducto(String buscar) {
        String[] datos = new String[4];
        SQLiteDatabase bd = this.getWritableDatabase();
        String q = "SELECT * FROM PRODUCTO WHERE TITULO ='"+buscar+"'";
        Cursor registros = bd.rawQuery(q,null);
        if (registros.moveToFirst()){
            for (int i = 0; i <= 3; i++) {
                datos[i] = registros.getString(i);
            }
        }
        return datos;
    }

    //CRUD PARA PROVEDORES
    public void agregarProvedor(String rsocial, String encargado, String telefono, String correo, String horario){
        SQLiteDatabase bd=getWritableDatabase();
        if (bd!=null){
            bd.execSQL("INSERT INTO PROVEDOR VALUES('"+rsocial+"', '"+encargado+"', '"+telefono+"','"+correo+"', '"+horario+"')");
            bd.close();
        }
    }

    public void eliminarProvedor(String id){
        SQLiteDatabase bd=getWritableDatabase();
        if (bd!=null){
            bd.execSQL("DELETE FROM PROVEDOR WHERE RS='"+id+"'");
            bd.close();
        }
    }

    public void actualizarProvedor(String nombre, String rsocial, String encargado, String telefono, String correo, String horario){
        SQLiteDatabase bd=getWritableDatabase();
        if (bd!=null){
            bd.execSQL("UPDATE PROVEDOR SET rs ='"+rsocial+"', encargado ='"+encargado+"', telefono ='"+telefono+"', correo ='"+correo+"' , horario ='"+horario+"' WHERE rs ='"+nombre+"'");
            bd.close();
        }
    }

    public ArrayList datos_lvProvedores(){
        ArrayList<String> listaP = new ArrayList<>();
        SQLiteDatabase bd = this.getReadableDatabase();
        String q = "SELECT * FROM PROVEDOR";
        Cursor registrosP = bd.rawQuery(q,null);
        if (registrosP.moveToFirst()){
            do {
                String provedor = registrosP.getString(0);
                listaP.add(provedor);
            }while(registrosP.moveToNext());
        }
        return listaP;
    }

    public  String[] buscaProvedor(String buscar) {
        String[] datos = new String[5];
        SQLiteDatabase bd = this.getWritableDatabase();
        String q = "SELECT * FROM PROVEDOR WHERE RS ='"+buscar+"'";
        Cursor registros = bd.rawQuery(q,null);
        if (registros.moveToFirst()){
            for (int i = 0; i <= 4; i++) {
                datos[i] = registros.getString(i);
            }
        }
        return datos;
    }
    // CRUD COMPRAS
    public void agregarCompra(String nombre, String telefono, String direccion, String tarjeta, String producto, String precio){
        SQLiteDatabase bd=getWritableDatabase();
        if (bd!=null){
            bd.execSQL("INSERT INTO COMPRAS VALUES('"+nombre+"', '"+telefono+"', '"+direccion+"','"+tarjeta+"', '"+producto+"', '"+precio+"')");
            bd.close();
        }
    }
    //HISTORIAL DE COMPRAS
    public ArrayList datos_lvCompras(){
        ArrayList<String> lista = new ArrayList<>();
        SQLiteDatabase bd = this.getReadableDatabase();
        String q = "SELECT * FROM COMPRAS";
        Cursor registros = bd.rawQuery(q,null);
        if (registros.moveToFirst()){
            do {
                String compra = registros.getString(0);
                lista.add(compra);
            }while(registros.moveToNext());
        }
        return lista;
    }

    public  String[] buscaVenta(String buscar) {
        String[] datos = new String[6];
        SQLiteDatabase bd = this.getWritableDatabase();
        String q = "SELECT * FROM COMPRAS WHERE nombre ='"+buscar+"'";
        Cursor registros = bd.rawQuery(q,null);
        if (registros.moveToFirst()){
            for (int i = 0; i <= 5; i++) {
                datos[i] = registros.getString(i);
            }
        }
        return datos;
    }
}
