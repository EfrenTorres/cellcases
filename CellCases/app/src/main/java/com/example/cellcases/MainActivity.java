package com.example.cellcases;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        try (GestorBD bd = new GestorBD(getApplicationContext())) {
            // guardar datos
             bd.agregarPre();
        }
    }
    public void ir_Tienda (View view){
        Intent irTienda = new Intent(MainActivity.this, Tienda.class);
        startActivity(irTienda);
    }
    public void ir_Admin (View view){
        Intent irAdmin = new Intent(MainActivity.this, Admin.class);
        startActivity(irAdmin);
    }

}