package com.example.cellcases;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class Atencion extends AppCompatActivity {

    private TextView txtLlamar;
    private  ImageButton btnLlamar;

    private final int PHONE_CALL_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atencion);

        txtLlamar = (TextView) findViewById(R.id.txtTelProv);
        btnLlamar = (ImageButton) findViewById(R.id.btnLlamarProv);

        btnLlamar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                String tel = txtLlamar.getText().toString();
                if (tel != null){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);
                    }else{
                        versionAntigua(tel);
                    }
                }
            }

            private void versiomNueva(){

            }
            private  void versionAntigua(String tel){
                Intent intentLlamar = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+tel));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)){
                    startActivity(intentLlamar);
                }else{
                    Toast.makeText(Atencion.this, "Active el permiso de llamadas", Toast.LENGTH_SHORT);
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result = grantResults[0];
                if (permission.equals(Manifest.permission.CALL_PHONE)){

                    if (result == PackageManager.PERMISSION_GRANTED){
                        String numTel = txtLlamar.getText().toString();
                        Intent llamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + numTel));
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) return;
                        startActivity(llamada);
                    }else {
                        Toast.makeText(this, "No haz autorizado el permiso", Toast.LENGTH_SHORT).show();
                    }
                }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private boolean verificarPermisos(String permiso){
        int resultado = this.checkCallingOrSelfPermission(permiso);
        return resultado == PackageManager.PERMISSION_GRANTED;
    }
}