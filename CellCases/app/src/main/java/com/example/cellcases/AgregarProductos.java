package com.example.cellcases;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AgregarProductos extends AppCompatActivity {

    EditText edtTitulo, edtMarca, edtPrecio;
    TextView edtDesc;
    Button btnAgregar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_productos);

        edtTitulo=(EditText)findViewById(R.id.etxtNombre);
        edtMarca=(EditText)findViewById(R.id.etxtMarca);
        edtPrecio=(EditText)findViewById(R.id.etxtPrecio);
        edtDesc=(TextView)findViewById(R.id.etxtDesc);
        btnAgregar=(Button)findViewById(R.id.btnGuardarProd);

        try (GestorBD bd = new GestorBD(getApplicationContext())) {

            btnAgregar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //verificar si los campos estan vacios
                    if (edtTitulo.getText().toString().isEmpty() || edtMarca.getText().toString().isEmpty() || edtPrecio.getText().toString().isEmpty() || edtDesc.getText().toString().isEmpty()) {
                        Toast.makeText(AgregarProductos.this, "ASEGURATE DE LLENAR TODOS LOS CAMPOS", Toast.LENGTH_LONG).show();
                    }else{
                        // guardar datos
                        bd.agregarProducto(edtTitulo.getText().toString(), edtMarca.getText().toString(), edtPrecio.getText().toString(), edtDesc.getText().toString());
                        Toast.makeText(getApplicationContext(), "SE HA AÑADIDO UN PRODUCTO", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }

            });
        }
    }
}