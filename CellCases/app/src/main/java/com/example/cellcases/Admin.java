package com.example.cellcases;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Admin extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
    }
    public void ir_Inventario (View view){
        Intent irInventario = new Intent(Admin.this, Inventario.class);
        startActivity(irInventario);
    }
    public void ir_Provedores (View view){
        Intent irProvedoes = new Intent(Admin.this, Provedores.class);
        startActivity(irProvedoes);
    }
    public void ir_Historial (View view){
        Intent irHistorial = new Intent(Admin.this, Historial.class);
        startActivity(irHistorial);
    }
}