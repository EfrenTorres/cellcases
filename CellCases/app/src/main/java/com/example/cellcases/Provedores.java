package com.example.cellcases;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class Provedores extends AppCompatActivity {

    ListView lvProvedores;
    ArrayList<String> lista;
    ArrayAdapter adaptador;

    @Override
    public void onResume() {
        super.onResume();
        setContentView(R.layout.activity_provedores);

        //configuracion lista y conexion a bd
        lvProvedores = (ListView) findViewById(R.id.lvProvedores);
        GestorBD bd = new GestorBD(getApplicationContext());
        lista = bd.datos_lvProvedores();
        adaptador = new ArrayAdapter(this, android.R.layout.simple_list_item_1,lista);
        lvProvedores.setAdapter(adaptador);

        //enviar nombre a detalle para hacer consulta
        lvProvedores.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> AdapterView, View view, int position, long l) {
                Intent intent = new Intent(Provedores.this, OpcionesProvedor.class);
                intent.putExtra("TITULO", lista.get(position));
                startActivity(intent);
            }
        });
    }
    public void ir_aProvedor (View view){
        Intent irAddProv = new Intent(Provedores.this, AgregarProvedor.class);
        startActivity(irAddProv);
    }
}