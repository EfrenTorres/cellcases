package com.example.cellcases;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class OpcionesProvedor extends AppCompatActivity {

    TextView txtRsocial, txtEncargado, txtTelefono, txtCorreo, txtHorario;
    Button btnModificar, btnEliminar;
    ImageButton btnLlamar;
    String id, telefono;
    private final int PHONE_CALL_CODE = 100;

    @Override
    public void onResume() {
        super.onResume();
        setContentView(R.layout.activity_opciones_provedor);

        txtRsocial=(TextView)findViewById(R.id.txtRsProv);
        txtEncargado=(TextView)findViewById(R.id.txtEncProv);
        txtTelefono=(TextView)findViewById(R.id.txtTelProv);
        txtCorreo=(TextView)findViewById(R.id.txtMailProv);
        txtHorario=(TextView)findViewById(R.id.txtHoraProv);
        btnLlamar=(ImageButton)findViewById(R.id.btnLlamarProv);
        btnModificar=(Button)findViewById(R.id.btnModificarProv);
        btnEliminar=(Button)findViewById(R.id.btnEliminarProv);

        //recibo nombre desde el main
        Bundle bundle = getIntent().getExtras();
        String nombre = bundle.getString("TITULO");

        // configuracion y conexion a bd
        try (GestorBD bd = new GestorBD(getApplicationContext())) {
            String buscar = nombre;
            String[] datos;
            datos=bd.buscaProvedor(buscar);
            //mostrar la info almacenada en datos resultado de la consulta
            txtRsocial.setText(datos[0]);
            txtEncargado.setText(datos[1]);
            txtTelefono.setText(datos[2]);
            txtCorreo.setText(datos[3]);
            txtHorario.setText(datos[4]);

            // id para titulo del producto
            id = nombre;

            // declaracion de botones
            btnLlamar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String tel = txtTelefono.getText().toString();
                    if (tel != null){
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                            requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);
                        }else{
                            versionAntigua(tel);
                        }
                    }
                }

                private void versiomNueva(){

                }
                private  void versionAntigua(String tel){
                    Intent intentLlamar = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+tel));
                    if (verificarPermisos(Manifest.permission.CALL_PHONE)){
                        startActivity(intentLlamar);
                    }else{
                        Toast.makeText(OpcionesProvedor.this, "Active el permiso de llamadas", Toast.LENGTH_SHORT);
                    }
                }
            });

            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(OpcionesProvedor.this, ModificarProvedor.class);
                    intent.putExtra("TITULO", id);
                    startActivity(intent);
                }

            });

            btnEliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bd.eliminarProvedor(id);
                    Toast.makeText(getApplicationContext(), "SE HA ELIMINADO "+id, Toast.LENGTH_SHORT).show();
                    finish();
                }

            });
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result = grantResults[0];
                if (permission.equals(Manifest.permission.CALL_PHONE)){

                    if (result == PackageManager.PERMISSION_GRANTED){
                        String numTel = txtTelefono.getText().toString();
                        Intent llamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + numTel));
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) return;
                        startActivity(llamada);
                    }else {
                        Toast.makeText(this, "No haz autorizado el permiso", Toast.LENGTH_SHORT).show();
                    }
                }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private boolean verificarPermisos(String permiso){
        int resultado = this.checkCallingOrSelfPermission(permiso);
        return resultado == PackageManager.PERMISSION_GRANTED;
    }
}