package com.example.cellcases;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class Inventario extends AppCompatActivity {

    ListView lvProductos;
    ArrayList<String> lista;
    ArrayAdapter adaptador;

    @Override
    public void onResume() {
        super.onResume();
        setContentView(R.layout.activity_inventario);

        //configuracion lista y conexion a bd
        lvProductos = (ListView) findViewById(R.id.lvProductos);
        GestorBD bd = new GestorBD(getApplicationContext());
        lista = bd.datos_lvProducto();
        adaptador = new ArrayAdapter(this, android.R.layout.simple_list_item_1,lista);
        lvProductos.setAdapter(adaptador);

        //enviar nombre a detalle para hacer consulta
        lvProductos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> AdapterView, View view, int position, long l) {
                Intent intent = new Intent(Inventario.this, ModificarProducto.class);
                intent.putExtra("TITULO", lista.get(position));
                startActivity(intent);
            }
        });
    }
    public void ir_aProducto (View view){
        Intent irAddP = new Intent(Inventario.this, AgregarProductos.class);
        startActivity(irAddP);
    }
}