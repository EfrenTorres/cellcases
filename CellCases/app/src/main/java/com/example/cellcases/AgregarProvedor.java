package com.example.cellcases;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AgregarProvedor extends AppCompatActivity {

    EditText edtRsocial, edtEncargado, edtTelefono, edtCorreo;
    TextView edtHorario;
    Button btnAgregarProv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_provedor);

        edtRsocial=(EditText)findViewById(R.id.etxtRsocialProv);
        edtEncargado=(EditText)findViewById(R.id.etxtEncargadoProv);
        edtTelefono=(EditText)findViewById(R.id.etxtTelefonoProv);
        edtCorreo=(EditText)findViewById(R.id.etxtCorreoProv);
        edtHorario=(TextView)findViewById(R.id.etxtHorarioProv);
        btnAgregarProv=(Button)findViewById(R.id.btnNuevoP);

        try (GestorBD bd = new GestorBD(getApplicationContext())) {

            btnAgregarProv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //verificar si los campos estan vacios
                    if (edtRsocial.getText().toString().isEmpty() || edtEncargado.getText().toString().isEmpty() || edtTelefono.getText().toString().isEmpty() || edtCorreo.getText().toString().isEmpty() || edtHorario.getText().toString().isEmpty()) {
                        Toast.makeText(AgregarProvedor.this, "ASEGURATE DE LLENAR TODOS LOS CAMPOS", Toast.LENGTH_LONG).show();
                    }else{
                        // guardar datos
                        bd.agregarProvedor(edtRsocial.getText().toString(), edtEncargado.getText().toString(), edtTelefono.getText().toString(), edtCorreo.getText().toString(), edtHorario.getText().toString());
                        Toast.makeText(getApplicationContext(), "SE HA AÑADIDO UN NUEVO PROVEDOR", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }

            });
        }
    }
}